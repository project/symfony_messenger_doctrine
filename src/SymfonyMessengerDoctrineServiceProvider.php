<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_doctrine;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Service provider for Symfony Messenger Doctrine.
 */
final class SymfonyMessengerDoctrineServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container
      // Run before 'Senders Service Locator' is built in
      // SymfonyMessengerCompilerPass (priority: 0).
      ->addCompilerPass(new SymfonyMessengerDoctrineCompilerPass(), priority: 100);
  }

}
