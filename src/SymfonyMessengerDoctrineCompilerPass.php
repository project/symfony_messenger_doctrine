<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_doctrine;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * Symfony Messenger Doctrine compiler pass.
 */
final class SymfonyMessengerDoctrineCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    $transports = $container->getParameter('symfony_messenger_doctrine.transports');

    // Similar to symfony/framework-bundle/DependencyInjection/FrameworkExtension.php:2192
    // Transports would normally be transformed from YAML to services.
    foreach ($transports as $name => $transport) {
      $serializerId = $transport['serializer'] ?? 'messenger.default_serializer';
      $transportDefinition = (new Definition(TransportInterface::class))
        ->setFactory([new Reference('messenger.transport_factory'), 'createTransport'])
        ->setArguments([
          $transport['dsn'],
          $transport['options'] + ['transport_name' => $name],
          new Reference($serializerId),
        ])
        ->addTag('messenger.receiver', [
          'alias' => $name,
          'is_failure_transport' => FALSE,
        ]);
      $container->setDefinition('symfony_messenger_doctrine.transport.' . $name, $transportDefinition);
    }
  }

}
