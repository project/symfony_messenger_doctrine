<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_doctrine;

use Doctrine\Persistence\ConnectionRegistry;

/**
 * Interface for handler responses.
 *
 * Temporary until https://www.drupal.org/project/dbal/issues/3389544
 *
 * @internal
 */
final class DoctrineConnectionRegistry implements ConnectionRegistry {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private array $connections
  )
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultConnectionName(): string {
    return array_key_first($this->connections);
  }

  /**
   * {@inheritdoc}
   */
  public function getConnection(?string $name = null): object {
    if ($name === NULL) {
      return $this->connections[array_key_first($this->connections)] ?? throw new \LogicException('Missing default connection');
    }

    return $this->connections[$name] ?? throw new \LogicException('Missing default connection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConnections(): array {
    return $this->connections;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionNames(): array {
    return array_keys($this->connections);
  }

}
